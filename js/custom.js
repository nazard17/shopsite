$(document).ready(function(){
    function acord() {
        $('.articl-menu-phone').not($(this).next()).fadeOut().removeClass('show').hide();
        $(this).next().addClass('show').show();
    };
    $('.phone-but').on('click', acord);
  $(function(){
	$('.header-page-slide').slick({
		dots: true,
		arrows:true,
		appendArrows:'.header-page-slide-buttons',
		prevArrow: '<button type="button" class="slick-prev"><i class="fa fa-chevron-left" aria-hidden="true"></i></button>',
		nextArrow: '<button type="button" class="slick-next"><i class="fa fa-chevron-right" aria-hidden="true"></i></button>',
		responsive: [

		]
	});
	$('.new-product-list-item').slick({
		dots: false,
		arrows:true,
		appendArrows:'.new-product-list-item-buttons',
		prevArrow: '<button type="button" class="slick-prev"><i class="fa fa-chevron-left" aria-hidden="true"></i></button>',
		nextArrow: '<button type="button" class="slick-next"><i class="fa fa-chevron-right" aria-hidden="true"></i></button>',
		responsive: [

		]
	});
	$('.top-product-list-item').slick({
		dots: false,
		arrows:true,
		appendArrows:'.top-buttons',
		prevArrow: '<button type="button" class="slick-prev"><i class="fa fa-chevron-left" aria-hidden="true"></i></button>',
		nextArrow: '<button type="button" class="slick-next"><i class="fa fa-chevron-right" aria-hidden="true"></i></button>',
		responsive: [

		]
	});
	$('.top-product-list-item-product-page').slick({
		dots: false,
		arrows:true,
		appendArrows:'.top-buttons-product-page',
		prevArrow: '<button type="button" class="slick-prev"><i class="fa fa-chevron-left" aria-hidden="true"></i></button>',
		nextArrow: '<button type="button" class="slick-next"><i class="fa fa-chevron-right" aria-hidden="true"></i></button>',
		responsive: [

		]
	});
	$('.sale-product-list-item').slick({
		dots: false,
		arrows:true,
		appendArrows:'.sale-buttons',
		prevArrow: '<button type="button" class="slick-prev"><i class="fa fa-chevron-left" aria-hidden="true"></i></button>',
		nextArrow: '<button type="button" class="slick-next"><i class="fa fa-chevron-right" aria-hidden="true"></i></button>',
		responsive: [
		]
	});
	$('.sale-product-list-item-product-page').slick({
		dots: false,
		arrows:true,
		appendArrows:'.sale-buttons-product-page',
		prevArrow: '<button type="button" class="slick-prev"><i class="fa fa-chevron-left" aria-hidden="true"></i></button>',
		nextArrow: '<button type="button" class="slick-next"><i class="fa fa-chevron-right" aria-hidden="true"></i></button>',
		responsive: [
		]
	});
	$('.slider-single').slick({
	   slidesToShow: 1,
	   slidesToScroll: 1,
	   arrows: true,
	   appendArrows:'.column-slider-single-buttons',
	   prevArrow: '<button type="button" class="slick-prev"><i class="fa fa-chevron-left" aria-hidden="true"></i></button>',
	   nextArrow: '<button type="button" class="slick-next"><i class="fa fa-chevron-right" aria-hidden="true"></i></button>',
	   fade: false,
	   adaptiveHeight: true,
	   infinite: false,
	   useTransform: true,
	   speed: 400,
	   cssEase: 'cubic-bezier(0.77, 0, 0.18, 1)',
});

$('.slider-nav')
   .on('init', function(event, slick) {
	   $('.slider-nav .slick-slide.slick-current').addClass('is-active');
   })
   .slick({
	   slidesToShow: 7,
	   slidesToScroll: 7,
	   dots: false,
	   focusOnSelect: false,
	   infinite: false,
	   responsive: [{
		   breakpoint: 1024,
		   settings: {
			   slidesToShow: 5,
			   slidesToScroll: 5,
		   }
	   }, {
		   breakpoint: 640,
		   settings: {
			   slidesToShow: 4,
			   slidesToScroll: 4,
		   }
	   }, {
		   breakpoint: 420,
		   settings: {
			   slidesToShow: 3,
			   slidesToScroll: 3,
	   }
	   }]
   });
		$('.slider-single').on('afterChange', function(event, slick, currentSlide) {
		   $('.slider-nav').slick('slickGoTo', currentSlide);
		   var currrentNavSlideElem = '.slider-nav .slick-slide[data-slick-index="' + currentSlide + '"]';
		   $('.slider-nav .slick-slide.is-active').removeClass('is-active');
		   $(currrentNavSlideElem).addClass('is-active');
		});

		$('.slider-nav').on('click', '.slick-slide', function(event) {
		   event.preventDefault();
		   var goToSingleSlide = $(this).data('slick-index');

		   $('.slider-single').slick('slickGoTo', goToSingleSlide);
		});
		  $('ul li a').click(function(){
		    $('li a').removeClass("active");
		    $(this).addClass("active");
		});
		$('.qtyplus').click(function(e) {
			  e.preventDefault();
			  fieldName = $(this).attr('rel');
			  var currentVal = parseInt($('input[name=' + fieldName + ']').val());
			  if (!isNaN(currentVal) && currentVal < 1000) {
				$('input[name=' + fieldName + ']').val(currentVal + 1);
			  } else {
				$('input[name=' + fieldName + ']').val(1000);
			  }
			});
		$('ul.tabs li').click(function(){
			var tab_id = $(this).attr('data-tab');
			$('ul.tabs li').removeClass('current');
			$('.tab-content').removeClass('current');
			$(this).addClass('current');
			$("#"+tab_id).addClass('current');
		});
	});
});
